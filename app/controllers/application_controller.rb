class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user
  before_action :current_user

  private

  def current_user
    uri = URI.parse("http://localhost:3000/users/#{session[:user_id]}?access_id=#{session[:user_id]}")
    json_user = Net::HTTP.get_response(uri)
    if json_user.code == "200"
      parsed_json_user = JSON.parse(json_user.body)
      @current_user ||= parsed_json_user if session[:user_id]
    else
      return false
    end
  end

  def get_users
    uri = URI("http://localhost:3000/users/")
    uri.query = URI.encode_www_form({:access_id => session[:user_id]})
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      users = JSON.parse(res.body)
    end

    return users
  end

  def get_experiences
    uri = URI("http://localhost:3000/experiences/")
    uri.query = URI.encode_www_form({:access_id => session[:user_id]})
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      experiences = JSON.parse(res.body)
    end

    return experiences
  end

  def get_cities
    uri = URI("http://localhost:3000/cities/")
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      cities = JSON.parse(res.body)
    end

    return users
  end

  def get_countries
    uri = URI("http://localhost:3000/countries/")
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      countries = JSON.parse(res.body)
    end

    return countries
  end

  def get_categories
    uri = URI("http://localhost:3000/categories/")
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      categories = JSON.parse(res.body)
    end

    return categories
  end




end
