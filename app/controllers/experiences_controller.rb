class ExperiencesController < ApplicationController
  #index, show, new, create, edit, update, delete

  def index
    @experiences = get_experiences
    puts "HELLO EXPERIENCES HERE"
    puts @experiences
  end

  def new
  end

  def create
  end

  def edit
    experiences = get_experiences

    experiences.each do |e|
      if e["id"].to_i == params[:id].to_i
        @cexperience = e
      end
    end
  end

  def update
    http = Net::HTTP.new("127.0.0.1", "3000")
    request = Net::HTTP::Put.new("/users/#{session[:user_id]}")

    pms = set_pms

    request.set_form_data(pms)
    response = http.request(request)
    redirect_to :back
  end

  def destroy

    http = Net::HTTP.new("127.0.0.1", 3000)
    request = Net::HTTP::Delete.new("/users/#{params[:id]}")
    request.set_form_data({:access_id => session[:user_id]})
    response = http.request(request)

    if response.is_a?(Net::HTTPSuccess)
      puts "User deleted successfully!"
      redirect_to '/users'
    else
      puts "Failed deleting user"
      redirect_to :back
    end
  end

  def confirm_delete
    users = get_users

    users.each do |u|
      if u["id"].to_i == params[:id].to_i
        @cuser = u
      end
    end
  end

end
