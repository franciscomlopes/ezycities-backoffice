class UsersController < ApplicationController
  #index, show, new, create, edit, update, delete

  def index
    @users = get_users
  end

  def new
  end

  def create
  end

  def edit
    users = get_users

    users.each do |u|
      if u["id"].to_i == params[:id].to_i
        @cuser = u
      end
    end

    if @cuser && @cuser["birth_date"]
      birth_date_array = @cuser["birth_date"].split("-")
      @cuser["birth_date"] = Date.new(birth_date_array[0].to_i, birth_date_array[1].to_i, birth_date_array[2].to_i)
    end
  end

  def update
    http = Net::HTTP.new("127.0.0.1", "3000")
    request = Net::HTTP::Put.new("/users/#{params[:id]}")

    pms = set_pms

    puts "HERE ,,,,"
    puts pms


    request.set_form_data(pms)
    response = http.request(request)
    redirect_to :back
  end

  def destroy

    http = Net::HTTP.new("127.0.0.1", 3000)
    request = Net::HTTP::Delete.new("/users/#{params[:id]}")
    request.set_form_data({:access_id => session[:user_id]})
    response = http.request(request)

    if response.is_a?(Net::HTTPSuccess)
      puts "User deleted successfully!"
      redirect_to '/users'
    else
      puts "Failed deleting user"
      redirect_to :back
    end
  end

  def confirm_delete
    users = get_users

    users.each do |u|
      if u["id"].to_i == params[:id].to_i
        @cuser = u
      end
    end
  end

  private

  def set_pms

    birth_date = Date.new(params["user"]["birthday(1i)"].to_i, params["user"]["birthday(2i)"].to_i, params["user"]["birthday(3i)"].to_i)

    pms = {:access_id => session[:user_id],
      :first_name => params[:user][:first_name],
      :last_name => params[:user][:last_name],
      :email => params[:user][:email],
      :stripe_account_id => params[:user][:stripe_account_id],
      :bio => params[:user][:bio],
      :city => params[:user][:city],
      :country => params[:user][:country],
      :phone_number => params[:user][:phone_number],
      :gender => params[:user][:gender],
      :role => params[:user][:role],
      :birth_date => birth_date
    }


    return pms
  end
end
