class HomeController < ApplicationController
  #index, show, new, create, edit, update, delete

  def index
    puts "HELLO FRIENDS"
    puts session[:user_id]
    if session[:user_id]
      render "home"
    else
      render "index"
    end
  end

  def home
    if session[:role] == 'admin'
      render "home"
    else
      redirect_to :root
    end
  end

  def email_for_password_v
    render "email_input"
  end

  def reset_email
    uri = URI.parse("http://localhost:3000/api/v1/auth/password")
    response = Net::HTTP.post_form(uri, {"email" => params['user']['email'], "redirect_url" => "http://localhost:4000/reset_password"})

    redirect_to :root
  end

  def password_input_v
  end




end
