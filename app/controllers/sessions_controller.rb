class SessionsController < ApplicationController
  #index, show, new, create, edit, update, delete


  def create

    uri = URI.parse("http://localhost:3000/api/v1/auth/sign_in")
    response = Net::HTTP.post_form(uri, {"email" => params['user']['email'], "password" => params['user']['password']})

    if response.is_a?(Net::HTTPSuccess)
      juser = JSON.parse(response.body)
      if juser["data"]["role"] == 'admin' || juser["data"]["role"] == 'Admin'
        session[:role] = juser["data"]["role"]
        session[:first_name] = juser["data"]["first_name"]
        session[:last_name] = juser["data"]["last_name"]
        session[:auth_token] = response["access-token"]
        session[:client] = response["client"]
        session[:user_id] = juser["data"]["id"]
        puts "SUCCESS!!!!"
        redirect_to "/home"
      else
        puts "NOPE , NOT AN ADMIN"
        redirect_to :back
      end
    else
      redirect_to :back
    end
  end

  def destroy

    #"ABRE" SITE
    uri = URI('http://localhost:3000/api/v1/auth/sign_out')
    http = Net::HTTP.new(uri.host, uri.port)

    #ESTABELECE O PEDIDO (HEADERS)
    req = Net::HTTP::Delete.new(uri.path)
    req["uid"] = session[:uid]
    req["access-token"] = session[:auth_token]
    req["client"] = session[:client]

    #FAZ O PEDIDO
    res = http.request(req)

    if res.is_a?(Net::HTTPSuccess)
      reset_session
      redirect_to :root
    else
      reset_session
      redirect_to :root
    end
  end


end
